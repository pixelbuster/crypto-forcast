angular.module("forecastApp",['main']);

angular.module("main",[]);

 angular.module("main").controller("forecastController",function($scope, $http){
   $scope.coinData = null;
   // $scope.error = 'Coin not found';
   $scope.getCoinsList = function(){
     var url = 'https://infinite-depths.herokuapp.com/coins'
     $http.get(url).then(function(response){
       console.log('url', response);
       $scope.allCoins = response.data.coins;
     })
   }
   $scope.getCoinData = function(coincode){
     $scope.loading = true;
     var url = 'https://infinite-depths.herokuapp.com/forecast?code=' + coincode;
     console.log('sure', url);
     $http.get(url).then(function(response){
       $scope.coinData=response.data.forecast;
       $scope.chartData = $scope.coinData.map(function(u){
         return u.usd
       })
       $scope.loading = false;
     })

   }
   $scope.getCodeFromList = function(query){
     $scope.result = null;
     $scope.coinData = null;
     $scope.error = '';
     var BreakExecption = {};
     for(var key in $scope.allCoins){
       try {
         if(key == query || query == $scope.allCoins[key].name.toLowerCase()){
           $scope.result = $scope.allCoins[key];
           $scope.coinName = $scope.allCoins[key].name;
           $scope.getCoinData($scope.allCoins[key].ticker)
           throw BreakExecption;
         } else {
           $scope.error = "coin not found";
         }
       } catch (e) {
         if(e!=BreakExecption){
           console.log('serious', e);
           $scope.error = e;
           throw e
         }
       }
     }
   }
});
